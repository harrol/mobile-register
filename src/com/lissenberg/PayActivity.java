package com.lissenberg;

import java.io.IOException;

import com.lissenberg.messaging.Sender;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PayActivity extends Activity {

	private EditText amount, description, response;
	private Handler handler;


	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		amount = (EditText) findViewById(R.id.amount);
		description = (EditText) findViewById(R.id.description);
		response = (EditText) findViewById(R.id.response);
		handler = new Handler();
		Button button = (Button) findViewById(R.id.pay);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				handler.post(
						new Runnable() {
							@Override
							public void run() {
								Sender s = new Sender(response, response);
								try {
									s.connectAndRegister();
									s.requestCard();
									s.pay(amount.getText().toString(), description.getText().toString());
									s.disconnect();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						});
			}
		});

	}
}