package com.lissenberg.messaging;

/**
 * @author Harro Lissenberg
 */
public class Config {

	public static final String HOST_IP = "127.0.0.1";
	public static final int HOST_PORT = 5557;
	public static final String CASH_REGISTER_ID = "mark";

}
